import React, { Component } from 'react';

class Sort extends Component {

    constructor(props) {
        super(props);
        //props.sortBy, sortDir, handleSort()
        this.state = {
            textSort: 'name - asc'
        }
    }

    handleSort = (sortBy, sortDir) => {
        let text = sortBy + ' - ' + sortDir;
        this.setState({
            textSort: text
        });
        this.props.handleSort(sortBy, sortDir);
    }

    render() {
        return (
            <div className="sort fl-l mar-l5">
                <div className="btn-group">
                    <button type="button" className="btn btn-success dropdown-toggle" data-bs-toggle="dropdown" >{this.state.textSort}</button>
                    <ul className="dropdown-menu">
                        <li><a onClick={() => this.handleSort('name', 'asc')} className="dropdown-item" role="button">name - asc</a></li>
                        <li><a onClick={() => this.handleSort('name', 'desc')} className="dropdown-item" role="button">name - desc</a></li>
                        <li><a onClick={() => this.handleSort('level', 'asc')} className="dropdown-item" role="button">level - asc</a></li>
                        <li><a onClick={() => this.handleSort('level', 'desc')} className="dropdown-item" role="button">level - desc</a></li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default Sort;
