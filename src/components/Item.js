import React, { Component } from 'react';

class Item extends Component {

  constructor(props) {
    super(props);
  }

  handleDelete = (id) => {
    this.props.handleDelete(id);
  }

  handleEdit = (member) => {
    this.props.handleEdit(member);
  }

  render() {
    let { member, index } = this.props;
    // console.log(member);
    return (
      <tr>
        <th scope="row">{index + 1}</th>
        <td>{member.name}</td>
        <td>{member.level}</td>
        <td>
          <button onClick={() => this.handleDelete(member.id)} type="button" className="btn btn-danger">Delete</button>
          <button onClick={() => this.handleEdit(member)} type="button" className="btn btn-info">Edit</button>
        </td>
      </tr>
    )
  }
}

export default Item;
