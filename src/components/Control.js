import React, { Component } from 'react';
import Search from './Search';
import Sort from './Sort';

class Control extends Component {

  constructor(props) {
    super(props);
    // this.props.onClickShowForm
    //this.props.onClickSearch
    //props.sortBy, sortDir
  }

  onClickShowForm = () => {
    this.props.onClickShowForm();
  }

  render() {
    return (
      <div className="control mar-t36">
        <Search 
          onClickSearch={this.props.onClickSearch} 
          onClickClear={this.props.onClickClear}></Search>
        <Sort
          sortBy={this.props.sortBy}
          sortDir={this.props.sortDir}
          handleSort={this.props.handleSort}></Sort>
        <button onClick={this.onClickShowForm} type="button" className="btn btn-success w-40">Success</button>
      </div >
    )
  }
}

export default Control;
