import React, { Component } from 'react';

class Form extends Component {

    constructor(props) {
        super(props);

        this.state = {
            member_id: '',
            member_name: '',
            member_level: 0
        }
    }

    componentWillMount() {
        if (this.props.memberEdit) {
            // console.log(member);
            let member = this.props.memberEdit;
            if (member.id !== '') {
                this.setState({
                    member_id: member.id,
                    member_name: member.name,
                    member_level: member.level
                });
            }
        }

    }

    handleChange = (event) => {
        this.setState({ value: event.target.value });
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    handleSubmit = (name, level) => {
        let member = {
            id: this.state.member_id,
            name: name,
            level: level
        }
        this.props.handleAdd(member);
        // event.preventDefault();
    }

    handleCancle = () => {
        this.props.handleCancle();
    }

    onChangeLevel = (level) => {
        this.setState({
            member_level: level
        });
    }

    render() {

        return (
            <div className="form w-45 fl-r mar-t10">
                <form className="padd-18">
                    <div className="col-auto w-50 dis-inb">
                        <input type="text" className="form-control"
                            onChange={this.handleChange}
                            value={this.state.member_name}
                            name="member_name"></input>
                    </div>
                    <div className="btn-group col-auto dis-inb mar-l5">
                        <button type="button" className="btn btn-success dropdown-toggle" data-bs-toggle="dropdown"
                            name="member_level">
                            {this.state.member_level}</button>
                        <ul className="dropdown-menu">
                            <li><a onClick={() => this.onChangeLevel(0)} className="dropdown-item" role="button">0</a></li>
                            <li><a onClick={() => this.onChangeLevel(1)} className="dropdown-item" role="button">1</a></li>
                            <li><a onClick={() => this.onChangeLevel(2)} className="dropdown-item" role="button">2</a></li>
                        </ul>
                    </div>
                    <div className="col-auto dis-inb mar-l5">
                        <button type="button" className="btn btn-primary mb-3 mar-t10"
                            onClick={() => this.handleSubmit(this.state.member_name, this.state.member_level)}
                            onChange={this.handleChange}>Submit</button>
                        <button onClick={this.handleCancle} type="button" className="btn btn-primary mb-3 mar-t10">Cancle</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default Form;
