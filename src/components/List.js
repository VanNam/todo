import React, { Component } from 'react';
import Item from './Item';

class List extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    let { members } = this.props;

    let elmList = members.map((member, index) => {
      return <Item member={member} key={index} index={index} handleDelete={this.props.handleDelete} handleEdit={this.props.handleEdit}></Item>
    });

    return (
      <div className="list mar-t36">
        <table className="table">
          <thead>
            <tr>
              <th scope="col">stt</th>
              <th scope="col">name</th>
              <th scope="col">level</th>
              <th scope="col">option</th>
            </tr>
          </thead>
          <tbody>
            {elmList}
          </tbody>
        </table>
      </div>
    )
  }
}

export default List;
