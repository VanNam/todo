import React, { Component } from 'react';

class Search extends Component {
    constructor(props) {
        super(props);
        //props.onClickSearch
        this.state={
            strSearch: '',

        }
    }

    onSearch = () => {
        this.props.onClickSearch(this.state.strSearch);
    }

    onClear = () => {
        this.setState({
            strSearch: ''
        })
        this.props.onClickClear();
    }

    handleChange = (event)=> {
        this.setState({strSearch: event.target.value});
    }

    render() {
        return (
            <div className="search dis-inb fl-l w-40">
                <div className="input-group">
                    <input value={this.state.strSearch} type="text" className="form-control" onChange={this.handleChange}></input>
                    <button onClick={this.onSearch} type="button" className="btn btn-success">Search</button>
                    <button onClick={this.onClear} type="button" className="btn btn-warning">Clear</button>
                </div>
            </div>
        )
    }
}

export default Search;
