const { v4: uuidv4 } = require('uuid');

let member = [
    {
        id: uuidv4(),
        name: 'Nam',
        age: 23,
        level: 1
    },
    {
        id: uuidv4(),
        name: 'Loc',
        age: 24,
        level: 1
    },
    {
        id: uuidv4(),
        name: 'Son',
        age: 21,
        level: 0
    },
    {
        id: uuidv4(),
        name: 'Khanh',
        age: 24,
        level: 0
    },
    {
        id: uuidv4(),
        name: 'Hieu',
        age: 21,
        level: 2
    }
]

export default member;