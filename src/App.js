import React, { Component } from 'react';
import Title from './components/Title';
import Control from './components/Control';
import Form from './components/Form';
import List from './components/List';
// import Footer from './components/Footer';
import members from './mock/member';
import _ from 'lodash';

import './App.css';

const { v4: uuidv4 } = require('uuid');


class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isShowForm: false,
      members: members,
      strSearch: '',
      sortBy: '',
      sortDir: '',
      id: '',
      name: '',
      level: null,
      memberEdit: null
    }
  }

  handleShowForm = () => {
    this.setState({
      isShowForm: !this.state.isShowForm
    })
  }

  handleSearch = (str) => {
    this.setState({
      strSearch: str
    })
  }

  handleClear = () => {
    this.setState({
      strSearch: ''
    })
  }

  handleSort = (sortBy, sortDir) => {
    this.setState({
      sortBy: sortBy,
      sortDir: sortDir
    });
  }

  handleAdd = (member) => {
    console.log(member);
    if (member.id !== '') { //edit
      console.log('edit');
      let evens = _.remove(this.state.members, function (x) {
        return  x.id == member.id;
      })
      this.state.members.push({
        id: uuidv4(),
        name: member.name,
        level: member.level
      })
    }
    else {
      this.state.members.push({ //add
        id: uuidv4(),
        name: member.name,
        level: member.level
      });
      console.log('add');
      
    }
    this.setState({
      name: member.name,
      level: member.level,
      isShowForm: false
    });

  }

  handleDelete = (id) => {
    let membersOrigin = this.state.members;
    let membersDelete = _.remove(membersOrigin, function (member) {
      return member.id == id;
    });
    this.setState({
      members: membersOrigin
    });
  }

  handleEdit = (member) => {
    this.setState({
      isShowForm: true,
      memberEdit: member
    });
  }

  handleCancle = () => {
    this.setState({
      isShowForm: false
    });
  }

  render() {
    let { isShowForm, strSearch, sortBy, sortDir, memberEdit } = this.state;
    let membersOrigin = this.state.members;
    console.log();
    let members = [];
    let elmShowForm = null;
    if (isShowForm) {
      elmShowForm = <Form handleAdd={this.handleAdd} handleCancle={this.handleCancle} memberEdit={memberEdit}></Form>
    }

    members = _.filter(membersOrigin, (item) => {
      return _.includes(item.name.toLocaleLowerCase(), strSearch.toLocaleLowerCase());
    });

    members = _.orderBy(members, [sortBy], [sortDir]);

    return (
      <div className="App dis-in">
        <Title />

        <Control
          onClickShowForm={this.handleShowForm}
          onClickSearch={this.handleSearch}
          onClickClear={this.handleClear}
          sortBy={sortBy}
          sortDir={sortDir}
          handleSort={this.handleSort}
        ></Control>

        {elmShowForm}

        <List
          members={members}
          handleDelete={this.handleDelete}
          handleEdit={this.handleEdit}
        ></List>
      </div>
    );
  }
}

export default App;
